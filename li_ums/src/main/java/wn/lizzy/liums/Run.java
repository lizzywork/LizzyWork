package wn.lizzy.liums;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * 
 * Run.java
 * @author nicolas 2017年3月12日--上午11:22:39
 *
 */
@SpringBootApplication
public class Run  extends SpringBootServletInitializer 
{
	/**
	 * 重写构建方法，部署到tomcat上。
	 * @param application
	 * @return
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Run.class);
    }
	
    public static void main( String[] args )
    {
    	SpringApplication.run(Run.class, args);
    }
}
