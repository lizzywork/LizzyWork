package wn.lizzy.liums.system.basic.repository;

import org.springframework.data.repository.CrudRepository;
import wn.lizzy.liums.system.basic.entity.UserInfo;

/**
 * UserInfo持久化接口
 * UserInfoRepository.java
 * @author nicolas  2017年3月18日--上午11:15:42
 */
public interface UserInfoRepository extends CrudRepository<UserInfo,Long>{
   
    /**通过username查找用户信息;*/
    public UserInfo findByUsername(String username);
   
}
