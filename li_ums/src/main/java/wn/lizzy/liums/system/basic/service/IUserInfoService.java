package wn.lizzy.liums.system.basic.service;

import wn.lizzy.liums.system.basic.entity.UserInfo;

/**
 * 后台基础服务接口类
 * IUserInfoService.java
 * @author nicolas  2017年3月18日--上午11:17:53
 */
public interface IUserInfoService {
	/**通过username查找用户信息;*/
    public UserInfo findByUsername(String username);
}
