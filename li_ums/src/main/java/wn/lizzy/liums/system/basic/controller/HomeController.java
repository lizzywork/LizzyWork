package wn.lizzy.liums.system.basic.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import wn.lizzy.liums.system.basic.entity.UserInfo;

/**
 * 后台主页显示控制类
 * HomeController.java
 * @author nicolas  2017年3月18日--上午11:33:50
 */
@Controller
@RequestMapping("/system")
public class HomeController {
	 @RequestMapping({"/","/index"})
	    public String index(Map<String, Object> map){
		 	UserInfo userinfo = (UserInfo)SecurityUtils.getSubject().getPrincipal();
		 	map.put("userinfo", userinfo);
		 	System.out.println(userinfo.getName());
	       return"system/index";
	    }
	 
	 @RequestMapping(value="/login",method=RequestMethod.GET)
	    public String login(){
	       return"system/login";
	    }
	 @RequestMapping(value="/logout",method=RequestMethod.GET)
	    public String logout(){
			 Subject subject = SecurityUtils.getSubject();
			 if (subject.isAuthenticated()) {
				subject.logout(); // session 会销毁，在SessionListener监听session销毁，清理权限缓存
			}
	       return"system/login";
	    }
	// 登录提交地址和applicationontext-shiro.xml配置的loginurl一致。 (配置文件方式的说法)
	    @RequestMapping(value="/login",method=RequestMethod.POST)
	    public String login(HttpServletRequest request, Map<String, Object> map) throws Exception {
	       System.out.println("HomeController.login()");
	       // 登录失败从request中获取shiro处理的异常信息。
	       // shiroLoginFailure:就是shiro异常类的全类名.
	       String exception = (String) request.getAttribute("shiroLoginFailure");
	 
	       System.out.println("exception=" + exception);
	       String msg = "";
	       if (exception != null) {
	           if (UnknownAccountException.class.getName().equals(exception)) {
	              System.out.println("UnknownAccountException -- > 账号不存在：");
	              msg = "UnknownAccountException -- > 账号不存在：";
	           } else if (IncorrectCredentialsException.class.getName().equals(exception)) {
	              System.out.println("IncorrectCredentialsException -- > 密码不正确：");
	              msg = "IncorrectCredentialsException -- > 密码不正确：";
	           } else if ("kaptchaValidateFailed".equals(exception)) {
	              System.out.println("kaptchaValidateFailed -- > 验证码错误");
	              msg = "kaptchaValidateFailed -- > 验证码错误";
	           } else {
	              msg = "else >> "+exception;
	              System.out.println("else -- >" + exception);
	           }
	       }
	       map.put("msg", msg);
	       // 此方法不处理登录成功,由shiro进行处理.
	       return"system/login";
	    }
}
