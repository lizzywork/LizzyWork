package wn.lizzy.liums.system.basic.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "t_userdetail")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetail {
	@Id @GeneratedValue
	private Long ID;
	
	private Long rN11um;
	private String rSt22r1;
	private String rStr332;
}
