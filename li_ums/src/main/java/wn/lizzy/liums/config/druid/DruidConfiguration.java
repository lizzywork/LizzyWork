package wn.lizzy.liums.config.druid;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;

/**
 * 使用代码注册方式引入Druid
 * 这样的方式不需要添加注解：@ServletComponentScan
 * DruidConfiguration.java
 * @author nicolas  2017年3月19日--上午9:39:05
 */
@Configuration
public class DruidConfiguration {
	 /**
     * 注册StatViewServlet
     * @return
     */
    @Bean
    public ServletRegistrationBean DruidStatViewServlet(){
         //org.springframework.boot.context.embedded.ServletRegistrationBean提供类的进行注册.
    	ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(),"/druid/*");
    	//添加初始化参数：initParams
    	//白名单：
    	servletRegistrationBean.addInitParameter("allow","127.0.0.1");
    	//IP黑名单 (存在共同时，deny优先于allow) : 如果满足deny的话提示:Sorry, you are not permitted to view this page.
    	servletRegistrationBean.addInitParameter("deny","192.168.1.73");
    	//登录查看信息的账号密码.
    	servletRegistrationBean.addInitParameter("loginUsername","admin");
    	servletRegistrationBean.addInitParameter("loginPassword","123456");
    	//是否能够重置数据.
    	servletRegistrationBean.addInitParameter("resetEnable","false");
    	return servletRegistrationBean;
    }

    /**
     * 注册一个：filterRegistrationBean
     * @return
     */
    @Bean
    public FilterRegistrationBean druidStatFilter(){
    	FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
    	//添加过滤规则.
    	filterRegistrationBean.addUrlPatterns("/*");
    	//添加不需要忽略的格式信息.
    	filterRegistrationBean.addInitParameter("exclusions","*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
    	return filterRegistrationBean;
    }
    
    /**
     * 实例化DruidDataSource主数据源对象
     * Bean中加入名称
     * @return
     */
    @Bean(name = "PrimaryDataSource") 
    @Qualifier("PrimaryDataSource")
    @Primary //在同样的DataSource中，首先使用被标注的DataSource
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource primaryDataSource() {
    	return DataSourceBuilder.create().type(DruidDataSource.class).build();
    }
    
    /**
     * 实例化从数据源
     * @return
     */
    @Bean(name = "SecondaryDataSource") 
    @Qualifier("SecondaryDataSource")
    @ConfigurationProperties(prefix="spring.read")
    public DataSource secondaryDataSource() {
    	return DataSourceBuilder.create().type(DruidDataSource.class).build();
    }
    
    @Bean(name = "PrimaryJdbcTemplate")
    public JdbcTemplate mainJdbcTemplate(@Qualifier("PrimaryDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "SecondaryJdbcTemplate")
    public JdbcTemplate readJdbcTemplate(@Qualifier("SecondaryDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
