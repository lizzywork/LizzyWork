package wn.lizzy.liums.front.huge.entity;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "t_huge3")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HugeA {
	@Id
	private Long ID;
	
	private Long rnum;
	private String rstr1;
	private String rstr2;
	private String rstr3;
	@Temporal(TemporalType.TIMESTAMP)
	private Date rdate;
//	@Lob
//	private String rblob;
}
