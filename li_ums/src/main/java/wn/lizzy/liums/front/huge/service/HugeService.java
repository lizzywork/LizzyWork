package wn.lizzy.liums.front.huge.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import wn.lizzy.liums.front.huge.entity.HugeA;
import wn.lizzy.liums.front.huge.entity.HugeB;
import wn.lizzy.liums.front.huge.repository.HugeARepository;
import wn.lizzy.liums.front.huge.repository.HugeBRepository;

@Service
public class HugeService implements IHugeService{
	@Resource
	public HugeARepository hugeARepository;
	@Resource
	public HugeBRepository hugeBRepository;
	
	@Override
	public HugeA findHugeAByID(Long id) {
		return hugeARepository.findOne(id);
	}

	@Override
	public HugeB findHugeBByID(Long id) {
		return hugeBRepository.findOne(id);
	}

	@Override
	public List<HugeA> findHugeAByRNum(Long rnum) {
		return hugeARepository.findByRnum(rnum);
	}

	@Override
	public List<HugeB> findHugeBByRNum(Long rnum) {
		return hugeBRepository.findByRnum(rnum);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false, rollbackFor = { Exception.class })
	public Page<HugeA> getHugeAList(Map<String, Object> queryPrams, Pageable pageable) {
		return hugeARepository.findAll((root, query, cb) -> {
			Predicate predicate = cb.conjunction();
			List<Expression<Boolean>> expressions = predicate.getExpressions();
			
			// *****************  此处添加约束条件   start ***************************
////			int qdept = (int)queryPrams.get("qdept");
//			String code = (String)queryPrams.get("code");
//			String name = (String)queryPrams.get("name");
//			
////			if(qdept > 0) {
////				expressions.add(cb.equal((root.<Unit> get("unit")).get("unitId"), qdept));
////			}
//			if(!Strings.isNullOrEmpty(code)) {
//				expressions.add(cb.like(root.<String> get("code"), "%"+code+"%"));
//			}
//			if(!Strings.isNullOrEmpty(name)) {
//				expressions.add(cb.like(root.<String> get("name"), "%"+name+"%"));
//			}
			// *****************  此处添加约束条件   end ***************************
			Order[] order = new Order[1];
			order[0] = cb.asc(root.get("rnum"));
			query.orderBy(order);
			return predicate;
		} , pageable);
	}

}
