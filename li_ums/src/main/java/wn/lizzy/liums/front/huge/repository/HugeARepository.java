package wn.lizzy.liums.front.huge.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import wn.lizzy.liums.front.huge.entity.HugeA;
@Repository
public interface HugeARepository extends PagingAndSortingRepository<HugeA, Long>,JpaSpecificationExecutor<HugeA> {

	public List<HugeA> findByRnum(Long rnum);
}
