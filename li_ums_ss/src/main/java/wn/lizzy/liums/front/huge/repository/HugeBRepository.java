package wn.lizzy.liums.front.huge.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import wn.lizzy.liums.front.huge.entity.HugeB;
@Repository
public interface HugeBRepository extends JpaRepository<HugeB, Long>,JpaSpecificationExecutor<HugeB>{
	public List<HugeB> findByRnum(Long rnum);
}
