package wn.lizzy.liums.front.huge.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import wn.lizzy.liums.front.huge.entity.HugeA;
import wn.lizzy.liums.front.huge.service.IHugeService;

@Controller
@RequestMapping("/huge")
public class HugeController {
	private static final Logger logger = LogManager.getLogger(HugeController.class);
	@Resource
	private Environment env;
	@Resource
	private IHugeService hugeService;

	@RequestMapping("/table")
	public Callable<String> hugetable(String pageNo, Model model){
		return () -> {
			int pageNumber = 1;
			if(pageNo != null && pageNo.trim().length()>0 ){
				pageNumber = Integer.parseInt(pageNo);
			}
			logger.trace("/huge/table");
			int PAGE_SIZE = Integer.parseInt(env.getProperty("liums.basic.pageinfo"));
			Map<String,Object> queryPrams = new HashMap<String,Object>();
			Page<HugeA> datas =  hugeService.getHugeAList(queryPrams, new PageRequest(pageNumber-1, PAGE_SIZE));
			Long itemsCount = datas.getTotalElements();
			
			model.addAttribute("pageNo", pageNumber);
			model.addAttribute("pageSize", PAGE_SIZE);
			model.addAttribute("itemsCount", itemsCount);
			model.addAttribute("hugeA_datas", datas.getContent());
			return "front/table";
		};
    }
	
	@RequestMapping("/socket")
	public Callable<String> socket(String pageNo, Model model){
		return () -> {
			return "front/websocket";
		};
	}
}
