package wn.lizzy.liums.front.huge.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import wn.lizzy.liums.front.huge.entity.HugeA;
import wn.lizzy.liums.front.huge.entity.HugeB;

public interface IHugeService {
	public HugeA findHugeAByID(Long id);
	public HugeB findHugeBByID(Long id);
	
	public List<HugeA> findHugeAByRNum(Long rNum);
	public List<HugeB> findHugeBByRNum(Long rNum);
	public Page<HugeA> getHugeAList(Map<String,Object> queryPrams, Pageable pageable);
}
