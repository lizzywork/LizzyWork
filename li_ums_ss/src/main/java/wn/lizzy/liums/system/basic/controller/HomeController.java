package wn.lizzy.liums.system.basic.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 后台主页显示控制类
 * HomeController.java
 * @author nicolas  2017年3月18日--上午11:33:50
 */
@Controller
@RequestMapping("/system")
public class HomeController {
	 @RequestMapping({"/","/index"})
	    public String index(Map<String, Object> map){
		 	return"system/index";
	    }
	 
	 @RequestMapping(value="/login",method=RequestMethod.GET)
	    public String login(){
	       return"system/login";
	    }
	 @RequestMapping(value="/logout",method=RequestMethod.GET)
	    public String logout(){
			
	       return"system/login";
	    }
	// 登录提交地址和applicationontext-shiro.xml配置的loginurl一致。 (配置文件方式的说法)
	    @RequestMapping(value="/login",method=RequestMethod.POST)
	    public String login(HttpServletRequest request, Map<String, Object> map) throws Exception {
	       System.out.println("HomeController.login()");
	       
	       return"system/login";
	    }
}
