package wn.lizzy.liums.system.basic.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import wn.lizzy.liums.system.basic.entity.UserInfo;
import wn.lizzy.liums.system.basic.repository.UserInfoRepository;

/**
 * UserInfo数据实际操作类
 * UserInfoService.java
 * @author nicolas  2017年3月18日--上午11:17:47
 */
@Service
public class UserInfoService implements IUserInfoService{
   
    @Resource
    private UserInfoRepository userInfoRepository;
   
    @Override
    public UserInfo findByUsername(String username) {
       System.out.println("UserInfoServiceImpl.findByUsername()");
       return userInfoRepository.findByUsername(username);
    }
}
