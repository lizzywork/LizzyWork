package wn.lizzy.liums.auto.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
/**
 * 属性的值可以在application.properties中来直接设置，如果不设置的话默认为world
 * HelloServiceProperties.java
 * @author cuilijian  2017年3月24日--下午3:53:49
 *
 */
@ConfigurationProperties(prefix = "hello")
public class HelloServiceProperties {
	 private static final String MSG = "world";
	 private String msg = MSG;
	 public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
