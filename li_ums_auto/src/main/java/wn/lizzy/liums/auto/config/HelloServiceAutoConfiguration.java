package wn.lizzy.liums.auto.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import wn.lizzy.liums.auto.property.HelloServiceProperties;
import wn.lizzy.liums.auto.service.HelloService;

/**
 * @ConditionalOnClass注解主要是用来判断HelloService这个类在路径中是否存在，
 * 在存在且容器中没有该类的Bean的情况下系统会自动配置需要的Bean，
 * 
 * @ConditionalOnProperty注解则表示指定的属性要满足的条件，
 * 在helloService方法中我们则使用了HelloServiceProperties提供的参数。
 * 
 * HelloServiceAutoConfiguration.java
 * @author cuilijian  2017年3月24日--下午3:54:15
 *
 */
@Configuration
@EnableConfigurationProperties(HelloServiceProperties.class)
@ConditionalOnClass(HelloService.class)
@ConditionalOnProperty(prefix = "hello",value = "enable",matchIfMissing = true)
public class HelloServiceAutoConfiguration {
    @Autowired
    private HelloServiceProperties helloServiceProperties;
    @Bean
    public HelloService helloService() {
        HelloService helloService = new HelloService();
        helloService.setMsg(helloServiceProperties.getMsg());
        return helloService;
    }
}
