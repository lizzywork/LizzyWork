package wn.lizzy.essay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import wn.lizzy.essay.entity.Type;
@Repository
public interface TypeRepository extends JpaRepository<Type, Long>,JpaSpecificationExecutor<Type>{

}
