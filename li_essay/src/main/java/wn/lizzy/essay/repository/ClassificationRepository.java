package wn.lizzy.essay.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import wn.lizzy.essay.entity.Classification;
@Repository
public interface ClassificationRepository extends CrudRepository<Classification, Long>,JpaSpecificationExecutor<Classification>{

}
