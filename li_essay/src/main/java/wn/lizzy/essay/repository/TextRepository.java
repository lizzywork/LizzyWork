package wn.lizzy.essay.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import wn.lizzy.essay.entity.Text;
@Repository
public interface TextRepository extends PagingAndSortingRepository<Text, Long>,JpaSpecificationExecutor<Text>{

}
