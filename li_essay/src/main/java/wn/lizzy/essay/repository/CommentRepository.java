package wn.lizzy.essay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import wn.lizzy.essay.entity.Comment;
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>,JpaSpecificationExecutor<Comment>{

}
