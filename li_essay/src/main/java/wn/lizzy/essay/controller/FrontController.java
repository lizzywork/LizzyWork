package wn.lizzy.essay.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Strings;

import wn.lizzy.essay.entity.Text;
import wn.lizzy.essay.service.ClassificationService;
import wn.lizzy.essay.service.TextService;
import wn.lizzy.essay.service.TypeService;
import wn.lizzy.essay.util.RelativeDateFormat;

@Controller
public class FrontController {
	private static final Logger logger = LoggerFactory.getLogger(TextController.class);
	@Resource
	private Environment env;
	@Resource
	private TextService textService;
	@Resource
	private ClassificationService classificationService;
	@Resource
	private TypeService typeService;
	
	@RequestMapping(value = {"/","/index"} , method = RequestMethod.GET)
	public Callable<String> index(Model model){
		return () -> {
			logger.trace("/index");
			int PAGE_SIZE = Integer.parseInt(env.getProperty("essay.basic.pageinfo"));
			Map<String,Object> queryPrams = new HashMap<String,Object>();
			queryPrams.put("valid", 1);
			Page<Text> datas =  textService.getTextList(queryPrams, new PageRequest(0, PAGE_SIZE));
			for(Text r:datas.getContent()){
				r.setRelativedate(Text.getRelativedate(r));
			}
			Long itemsCount = datas.getTotalElements();
		
			model.addAttribute("types", typeService.getTypes());
			model.addAttribute("pageNo", 1);
			model.addAttribute("pageSize", PAGE_SIZE);
			model.addAttribute("itemsCount", itemsCount);
			model.addAttribute("text_datas", datas.getContent());
			return "front/index";
		};
    }
	
	@RequestMapping(value = "/page", method = RequestMethod.POST)
	public Callable<String> page(String title, int pageNo, Model model) {
		return () -> {
			logger.trace("/page");
			int PAGE_SIZE = Integer.parseInt(env.getProperty("essay.basic.pageinfo"));
			Map<String,Object> queryPrams = new HashMap<String,Object>();
			queryPrams.put("valid", 1);
			if(!Strings.isNullOrEmpty(title))
				queryPrams.put("title", title);
			Page<Text> datas =  textService.getTextList(queryPrams, new PageRequest(pageNo-1, PAGE_SIZE));
			for(Text r:datas.getContent()){
				r.setRelativedate(Text.getRelativedate(r));
			}
			Long itemsCount = datas.getTotalElements();
			
			model.addAttribute("types", typeService.getTypes());
			model.addAttribute("pageNo", pageNo);
			model.addAttribute("pageSize", PAGE_SIZE);
			model.addAttribute("itemsCount", itemsCount);
			model.addAttribute("text_datas", datas.getContent());
			return "front/index";
		};
	}
	
	@RequestMapping(value = "/view/{uid}" , method = RequestMethod.GET)
	public Callable<String> view(@PathVariable("uid") Long uid,Model model){
		return () -> {
			logger.trace("/view/"+uid);
			Text text = textService.getText(uid);
			model.addAttribute("relativedate", RelativeDateFormat.format(text.getUpdatetime()));
			model.addAttribute("text", text);
			return "front/view";
		};
	}
}
