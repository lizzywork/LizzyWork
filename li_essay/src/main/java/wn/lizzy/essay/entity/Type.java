package wn.lizzy.essay.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文章类型表
 * Type.java
 * @author cuilijian  2017年3月27日--上午10:54:31
 *
 */
@Entity
@Table(name = "essay_type")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Type {
	@Id @GeneratedValue
	private long uid;//用户id;
	
	@Column(unique=true,length=50)
	private String name;
	
	@Column(length=500)
	private String description;
	private Boolean valid = Boolean.FALSE;
}
