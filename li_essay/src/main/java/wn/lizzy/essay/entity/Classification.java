package wn.lizzy.essay.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 栏目分类表
 * Column.java
 * @author cuilijian  2017年3月27日--上午10:44:48
 *
 */
@Entity
@Table(name = "essay_classification")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Classification {
	@Id @GeneratedValue
	private long uid;

	@Column(unique=true,length=100)	
	private String name;//分类名称

	/**
	 * 父栏目id，一级分类 =0
	 */
	private long parent; 
	private Boolean valid = Boolean.FALSE;
}
