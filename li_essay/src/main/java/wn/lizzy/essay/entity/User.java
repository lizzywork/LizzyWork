package wn.lizzy.essay.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户信息
 * User.java
 * @author cuilijian  2017年3月27日--上午10:30:52
 *
 */

@Entity
@Table(name = "essay_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
	
	@Id @GeneratedValue
	private long uid;//用户id;
	
	@Column(unique=true,length=50)
	private String username;//账号.
	@Column(length=50)
	private String name;//名称（昵称或者真实姓名，不同系统不同定义）
	@Column(length=50)
	private String password; //密码; MD5(2)(名称+密码)校验
	@Column(length=500)
	private String description;
	private Boolean valid = Boolean.FALSE;
	
	@ManyToMany(fetch=FetchType.EAGER)//立即从数据库中进行加载数据;
    @JoinTable(name = "essay_userclass", joinColumns = { @JoinColumn(name = "uid") }, inverseJoinColumns ={@JoinColumn(name = "classid") })
    private List<Classification> classificationList;// 一个用户可管理的栏目列表
}
