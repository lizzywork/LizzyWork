package wn.lizzy.essay.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 评论表
 * Comment.java
 * @author cuilijian  2017年3月27日--上午11:03:25
 *
 */
@Entity
@Table(name = "essay_comment")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
	@Id @GeneratedValue
	private long uid;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "textid")
	private Text text;
	
	@Lob
	private String content;//评论正文
	
	/**
	 * 父评论id，一级评论=0
	 */
	private long parent; 
	private Boolean valid = Boolean.FALSE;
}
