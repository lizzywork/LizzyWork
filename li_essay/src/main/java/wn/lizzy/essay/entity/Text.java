package wn.lizzy.essay.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import wn.lizzy.essay.util.RelativeDateFormat;

/**
 * 正文表
 * Text.java
 * @author cuilijian  2017年3月27日--上午10:55:47
 *
 */
@Entity
@Table(name = "essay_text")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Text {

	@Id @GeneratedValue
	private long uid;
	
	@JsonBackReference
	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "classid")
	private Classification classification;
	
	@JsonBackReference
	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "typeid")
	private Type type;
	
	
	@Column(unique=true,length=200)	
	private String title;//文章标题
	
	@Column(length=500)	
	private String introduction;//文章摘要
	
	@Column(length=500)	
	private String thumbnail;//缩略图
	
	@Lob
	@Basic(fetch=FetchType.LAZY,optional=false) 
	@Column(length = 16777215)
	private String content;//正文
	
	@JsonBackReference
	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "createuser")
	private User createuser;
	
	@JsonBackReference
	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "updateuser")
	private User updateuser;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;
	
	@Transient
	private String relativedate;
	static public String getRelativedate(Text obj){
		return RelativeDateFormat.format(obj.getUpdatetime());
	}
	
	@Column(length = 1)
	private int valid;
	
	private Long clicknum;
}
