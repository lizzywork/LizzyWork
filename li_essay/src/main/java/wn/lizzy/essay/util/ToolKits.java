package wn.lizzy.essay.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.common.base.CharMatcher;

public class ToolKits {

	/**
	 * 通过通配符获得字符串列表
	 * @param str
	 * @param pdix
	 * @return
	 */
	public static List<String> StringsOper(String str,String pdix){
		List<String> list = new ArrayList<String>();
		String[] strs=str.split(pdix);
		for(int i=0;i<strs.length;i++){
			list.add(strs[i]);
		}
		return list;
	}
	
	public static String sqlStringReplace(String str){
		String replaceS=str;
		replaceS = CharMatcher.anyOf("%").replaceFrom(replaceS, "\\%");
		replaceS = CharMatcher.anyOf("?").replaceFrom(replaceS, "\\?");
		replaceS = CharMatcher.anyOf("'").replaceFrom(replaceS, "\\'");
		return replaceS;
	}
	
	/**
	 * 通过截止时间获取到此时间的小时数
	 * @param endTime
	 * @return
	 */
	public static long getPeriodHours(Date endTime){
		long hours=0;
		Date currentTime=new Date();
		//包含截止当天
		hours=(endTime.getTime()-currentTime.getTime())/(1000*60*60)+24;
		return hours;
	}
}
