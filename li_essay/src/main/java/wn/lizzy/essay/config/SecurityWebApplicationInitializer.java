package wn.lizzy.essay.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

// 注入 Spring Security 拦截器
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
