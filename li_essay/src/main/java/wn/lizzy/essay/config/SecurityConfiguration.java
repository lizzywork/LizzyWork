package wn.lizzy.essay.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

import com.google.common.collect.Lists;

/**
 * Spring Security配置
 * SecurityConfiguration.java
 * @author cuilijian  2017年3月27日--下午2:20:53
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
				.authorizeRequests()
					.antMatchers("/" , "/list/**", "/view/**","/druid/**","/css/**", "/images/**", "/js/**", "/plugins/**").permitAll()
					.anyRequest().authenticated()
					.and()
				.formLogin()
					.loginPage("/essay/login").permitAll()
					.and()
					.httpBasic().and().headers().frameOptions().sameOrigin();
	}
	
	// 权限验证点
		@Bean
		public LoginUrlAuthenticationEntryPoint authenticationEntryPoint() {
			return new LoginUrlAuthenticationEntryPoint("/essay/login");
		}

		// 默认访问点
		@Bean
		public AccessDeniedHandlerImpl accessDeniedHandler() {
			AccessDeniedHandlerImpl accessDeniedHandler = new AccessDeniedHandlerImpl();
			accessDeniedHandler.setErrorPage("/essay/index");
			return accessDeniedHandler;
		}

		// Spring Security 异常解析器
		@Bean
		public ExceptionTranslationFilter exceptionTranslationFilter(AuthenticationEntryPoint authenticationEntryPoint,
				AccessDeniedHandler accessDeniedHandler) {
			ExceptionTranslationFilter exceptionTranslationFilter = new ExceptionTranslationFilter(authenticationEntryPoint);
			exceptionTranslationFilter.setAccessDeniedHandler(accessDeniedHandler);
			return exceptionTranslationFilter;
		}

		// 将自定义用户登录校验规则添加到 ProviderManager
		@Bean
		public ProviderManager authenticationManager(AuthenticationProvider authenticationProvider) {
			List<AuthenticationProvider> providers = Lists.newArrayList(authenticationProvider);
			ProviderManager manager = new ProviderManager(providers);
			return manager;
		}

}
