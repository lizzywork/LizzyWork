package wn.lizzy.essay.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import wn.lizzy.essay.entity.Text;
import wn.lizzy.essay.repository.TextRepository;

@Service
public class FrontService {
	@Resource
	private TextRepository textRepository;
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = { Exception.class })
	public void updateClickNum(Long uid){
		Text text = textRepository.findOne(uid);
		text.setClicknum(text.getClicknum()+1);
		textRepository.save(text);
	}
}
