package wn.lizzy.essay.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import wn.lizzy.essay.entity.Type;
import wn.lizzy.essay.repository.TypeRepository;

@Service
public class TypeService {
	@Resource
	private TypeRepository typeRepository;
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Type> getTypes(){
		return (List<Type>) typeRepository.findAll();
	}
}
