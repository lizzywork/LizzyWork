package wn.lizzy.essay.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import wn.lizzy.essay.entity.Classification;
import wn.lizzy.essay.repository.ClassificationRepository;

@Service
public class ClassificationService {
	@Resource
	private ClassificationRepository classificationRepository;
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Classification> getClassification(){
		return (List<Classification>) classificationRepository.findAll();
	}
}
