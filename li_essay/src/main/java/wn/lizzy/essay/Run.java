package wn.lizzy.essay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动程序
 * @author nicolas  2017年3月26日--下午9:53:31
 *Run.java
 */
@SpringBootApplication
public class Run 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(Run.class, args);
    }
}
